package cz.cuni.gamedev.nail123.roguelike.utils;

import org.hexworks.zircon.api.data.Position3D;

public class Tree{
    public Tree leftChild;
    public Tree rightChild;
    public Box area;
    public Position3D splitPoint;

    public Tree(Box area)
    {
        this.area = area;
    }
}
