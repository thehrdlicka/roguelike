package cz.cuni.gamedev.nail123.roguelike.utils;

import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;

public class Box
{
    public Position3D point;
    public Size3D size;

    public Box(int x, int y, int w, int h)
    {
        size = Size3D.create(w,h,1);
        point = Position3D.create(x,y,0);
    }

    public int getStartX()
    {
        return point.getX();
    }
    public int getStartY()
    {
        return point.getY();
    }

    public int getEndX()
    {
        return point.getX() + size.getXLength();
    }
    public int getEndY()
    {
        return point.getY() + size.getYLength();
    }

    public int getWidth()
    {
        return size.getXLength();
    }

    public int getHeight()
    {
        return size.getYLength();
    }
}
