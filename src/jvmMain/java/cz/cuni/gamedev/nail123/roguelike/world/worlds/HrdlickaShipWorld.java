package cz.cuni.gamedev.nail123.roguelike.world.worlds;


import cz.cuni.gamedev.nail123.roguelike.arenabuilders.HrdlickaShipBuilder;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Human;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Cheese;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Fire;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Lighter;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent;
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.World;
import kotlin.random.Random;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class HrdlickaShipWorld extends World {
    int currentLevel = 0;

    /*
    Pocita s modifikovanou area size:
        val AREA_SIZE = Size3D.create(
            VISIBLE_WIDTH * 2,
            VISIBLE_HEIGHT * 2,
            1
    )
     */
    //Vyska radku ze kterejch se stavi lod
    final int SHIP_ROW_SIZE = 10;
    //minimalni velikost roomu
    final int MIN_ROOM_SIZE = 5;
    //Pocet iteraci binary divize.
    final int ITERATIONS = 10;

    public HrdlickaShipWorld() {
    }

    private int numberOfRats = 4;



    @NotNull
    @Override
    public Area buildStartingArea() {
        return buildLevel();
    }

    Area buildLevel() {
        // Generate a ship layout.
        HrdlickaShipBuilder areaBuilder = (new HrdlickaShipBuilder(SHIP_ROW_SIZE,MIN_ROOM_SIZE, ITERATIONS)).create();

        // Place the player at the Bridge.
        areaBuilder.addAtEmptyPosition(
                areaBuilder.getPlayer(),
                Position3D.create(areaBuilder.getWidth()/2 - SHIP_ROW_SIZE/2, 1, 0),
                Size3D.create(SHIP_ROW_SIZE, SHIP_ROW_SIZE/2, 1)
        );

        if(currentLevel == 0) {
            numberOfRats = 3;
            areaBuilder.addAtEmptyPosition(
                    new Lighter(3),
                    Position3D.create(areaBuilder.getWidth() / 2 - SHIP_ROW_SIZE / 2, 1, 0),
                    Size3D.create(SHIP_ROW_SIZE, SHIP_ROW_SIZE / 2, 1)
            );
        }

        for(int i=0;i<numberOfRats;i++) {
            areaBuilder.addAtEmptyPosition(
                    new Rat(),
                    Position3D.create(areaBuilder.getWidth() / 2 - SHIP_ROW_SIZE / 2, 1, 0),
                    Size3D.create(SHIP_ROW_SIZE, SHIP_ROW_SIZE / 2, 1)
            );
        }


        // Add stairs down
        Map<Position3D, Integer> floodFill = Pathfinding.INSTANCE.floodFill(
                areaBuilder.getPlayer().getPosition(),
                areaBuilder,
                Pathfinding.INSTANCE.getEightDirectional(),
                Pathfinding.INSTANCE.getDoorOpening()
        );

        Position3D[] exitPositions = {};
        exitPositions = floodFill.keySet().toArray(exitPositions);

        //Schody da nahodne do poslednich 30% nejvzdalenejsich mist od hrace.
        Position3D staircasePosition = exitPositions[Random.Default.nextInt((int)(exitPositions.length*0.75), exitPositions.length)];

        areaBuilder.addEntity(new Stairs(), staircasePosition);

        var enemySpawnSize = Size3D.create(areaBuilder.getWidth(),areaBuilder.getHeight() - SHIP_ROW_SIZE,1);
        var enemySpawnPoint = Position3D.create(0,SHIP_ROW_SIZE+2,0);
        for (int i = 0; i <= (currentLevel + 1) * 4; ++i) {
            areaBuilder.addAtEmptyPosition(new Human(), enemySpawnPoint, enemySpawnSize);
        }

        for (int i = 0; i <= (currentLevel + 1) * 4; ++i) {
            areaBuilder.addAtEmptyPosition(new Cheese(Random.Default.nextInt(5,11)), enemySpawnPoint, enemySpawnSize);
        }

        // Build it into a full Area
        return areaBuilder.build();
    }

    /**
     * Moving down - goes to a brand new level.
     */
    @Override
    public void moveDown() {
        int enemyLeft = 0;
        int ratsLeft = 0;
        for (var entity : getCurrentArea().getEntities()) {
            if(entity instanceof Enemy)
                enemyLeft++;
            if(entity instanceof Rat)
                ratsLeft++;
        }

        if(enemyLeft > 0)
        {
            (new LoggedEvent(this, "You need to kill everyone before leaving!")).emit();
            return;
        }

        ++currentLevel;
        numberOfRats = ratsLeft;
        (new LoggedEvent(this, "Teleported to ship number " + (currentLevel + 1))).emit();


        if (currentLevel >= getAreas().getSize()) getAreas().add(buildLevel());
        goToArea(getAreas().get(currentLevel));
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    @Override
    public void moveUp() {
        // Not implemented
    }



}

