package cz.cuni.gamedev.nail123.roguelike.arenabuilders;

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor;
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Airlock;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.AutomaticDoor;
import cz.cuni.gamedev.nail123.roguelike.utils.Box;
import cz.cuni.gamedev.nail123.roguelike.utils.Tree;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import kotlin.NotImplementedError;
import kotlin.random.Random;
import org.hexworks.zircon.api.data.Position3D;

import java.util.ArrayList;
import java.util.List;

public class HrdlickaShipBuilder extends AreaBuilder {
    private int shipRowSize;
    private int minRoomSize;
    private int iterations;
    private java.util.Random rand = new java.util.Random();


    public HrdlickaShipBuilder ( int rowSize, int roomSize, int iterations)
    {
        super();
        minRoomSize = roomSize;
        shipRowSize = rowSize;
        this.iterations = iterations;
    }

     public Area afterBuild(Area area)
    {
        area.rooms = roomList;
        return area;
    }

    private ArrayList<Box> roomList;

    private void fillRoomList(Tree ofTree)
    {
        if(ofTree==null)
            return;
        if(ofTree.rightChild == null && ofTree.leftChild == null)
            roomList.add(ofTree.area);
        else
        {
            fillRoomList(ofTree.leftChild);
            fillRoomList(ofTree.rightChild);
        }

    }

    private ArrayList<Box> getRoomsFromBox(Box box)
    {
        throw new NotImplementedError();
    }

    public ArrayList<Box> getRoomAreaAprox()
    {
        return roomList;
    }


    public HrdlickaShipBuilder create()  {
        buildShipRows();
        var tree = splitAreaTree(new Box(0,0,getWidth(),getHeight()),iterations);
        fillTreeWalls(tree);
        placeDoors(tree);
        roomList = new ArrayList<Box>();
        fillRoomList(tree);
        return this;
    }

    private int numberOfRows;
    //Nahodne vygeneruje layout lodi, podle sirky radku
    private void buildShipRows()
    {
        numberOfRows = (int)Math.floor(getHeight() / shipRowSize);
        int lastWidth = 0;
        for(int i = 0;i<numberOfRows;i++)
        {
            int width = getRowWidth(i);
            buildRow(shipRowSize*i,width,lastWidth);
            lastWidth = width;
        }
    }

    //Postavi radek. Nakresli ho celej, a obezene zdma. Nahore vynecha prostor tam,
    //kde sousedi, aby byl pruchozi (zdi uvnitr doda az binary division)
    private void buildRow(int startY, int width, int lastWidth)
    {
        int startX = (getWidth()/2) - (width/2);
        int endX = (getWidth()/2) + (width/2) ;
        int lastStart = (getWidth()/2) - (lastWidth/2);
        int lastEnd = (getWidth()/2) + (lastWidth/2) - 1;
        if(lastStart < startX)
            lastStart = startX;
        if(lastEnd >= endX)
            lastEnd = endX - 1;

        for(int y=startY;y<=startY+shipRowSize;y++) {
            for (int x = startX; x < endX; x++) {
                var isBorder = x == startX || x == endX - 1 || y == startY || y == startY+shipRowSize;
                var isRoomConnection = y==startY && x > lastStart && x < lastEnd;

                if (isBorder && !isRoomConnection) {
                    if(y - startY == shipRowSize /2) {
                        getBlocks().put(Position3D.create(x, y, 0), new Floor());
                        addEntity(new Airlock(), Position3D.create(x, y, 0));
                    }
                    else
                        getBlocks().put(Position3D.create(x, y, 0), new Wall());
                }
                else
                    getBlocks().put(Position3D.create(x, y, 0),new Floor());
            }
        }
    }
    //Tohle resi random mezi 0-1 s normalni distribuci.
    //Je to proto, abylode meli uzkej predek a tlustej zadek.
    private final double RAND_MEAN = 50;
    private final double RAND_DEV = 20;
    private double getNormalRandom(double meanOffset)
    {
        var result = rand.nextGaussian() * RAND_DEV + (RAND_MEAN + meanOffset);
        result = clamp(result, 0, 100);
        return result / 100;
    }

    //Tohle java fakt neumi? Heh.
    private double clamp(double number, double min, double max)
    {
        if(number < min)
            return min;
        if(number > max)
            return max;
        return number;
    }
    //Generuje sirku lodi nadanym radku na zaklade toho jak moc je to vzadu.
    private final int SIZE_MAX_DEV = 40;
    private int getRowWidth(int rowNumber)
    {            //Jak moc vepredu sme, od -1 do 1.
        double sizeScale = (((double)rowNumber / (double)numberOfRows) * 2) - 1;
        double sizeDeviation = sizeScale * SIZE_MAX_DEV;
        int result = (int)Math.floor(getNormalRandom(sizeDeviation) * (getWidth() - shipRowSize) + shipRowSize);

        if(result % 2 == 0)
            result++;
        return result;
    }

    private boolean isWall(int x, int y)
    {
        var block = getBlocks().get(Position3D.create(x, y, 0));
        return block == null || block.getBlocksMovement();
    }

    private boolean isBlock(int x, int y)
    {
        var block = getBlocks().get(Position3D.create(x, y, 0));
        return block != null;
    }

    private boolean canBeDoorsX(int x, int y)
    {
        return isFloor(x,y+1) && isFloor(x,y-1);
    }
    private boolean canBeDoorsY(int x, int y)
    {
        return isFloor(x+1,y) && isFloor(x-1,y);
    }

    private boolean isFloor(int x, int y)
    {
        var block = getBlocks().get(Position3D.create(x, y, 0));
        return block != null && !block.getBlocksMovement();
    }

    //Resi pridavani dveri. Protoze lod nejsou jenom obdelniky, ale ma
    //nejakej layout od zacatku kterej jenom rezeme, musi se zajistit ze kdyz
    //division vytvori 2 roomy, tak taky pridá dvoje dvere.
    private void placeDoors(Tree ofTree)
    {
        if(ofTree == null || ofTree.splitPoint == null)
            return;

        if(ofTree.splitPoint.getY() == -1)
            placeDoorX(ofTree.area,ofTree.splitPoint.getX());
        else
            placeDoorY(ofTree.area,ofTree.splitPoint.getY());

        placeDoors(ofTree.leftChild);
        placeDoors(ofTree.rightChild);
    }

    private void placeDoorX(Box toArea, int onX)
    {
        int lastFloorStart = -1;
        boolean doorsPlaced = true;
        for(int y = toArea.getStartY();y<toArea.getEndY();y++)
        {
            if(doorsPlaced)
            {
                if(isBlock(onX,y))
                {
                    lastFloorStart = y;
                    doorsPlaced = false;
                    continue;
                }
            }

            if(!doorsPlaced && !isBlock(onX,y))
            {
                doorsPlaced = true;
                int doorSpot;
                int tries = 0;
                do {
                    doorSpot = Random.Default.nextInt(lastFloorStart+1, y);
                    tries++;

                }while(tries<510 && !canBeDoorsY(onX,doorSpot));

                if(tries > 500)
                    continue;
                var doorPos = Position3D.create(onX,doorSpot,0);
                getBlocks().put(doorPos, new Floor());
                addEntity(new AutomaticDoor(), doorPos);

            }

        }

        if(!doorsPlaced)
        {
            doorsPlaced = true;
            int doorSpot;
            int tries = 0;
            do {
                doorSpot = Random.Default.nextInt(lastFloorStart+1, toArea.getEndY());
                tries++;

            }while(tries<510 && !canBeDoorsY(onX,doorSpot));

            if(tries > 500)
                return;
            var doorPos = Position3D.create(onX,doorSpot,0);
            getBlocks().put(doorPos, new Floor());
            addEntity(new AutomaticDoor(), doorPos);
        }
    }

    private void placeDoorY(Box toArea, int onY)
    {
        int lastFloorStart = -1;
        boolean doorsPlaced = true;
        for(int x = toArea.getStartX();x<toArea.getEndX();x++)
        {
            if(doorsPlaced)
            {
                if(isBlock(x,onY))
                {
                    lastFloorStart = x;
                    doorsPlaced = false;
                    continue;
                }
            }

            if(!doorsPlaced && !isBlock(x,onY))
            {
                doorsPlaced = true;
                int doorSpot;
                int tries = 0;
                do {
                    doorSpot = Random.Default.nextInt(lastFloorStart+1, x);
                    tries++;

                }while(tries<510 && !canBeDoorsX(doorSpot,onY));

                if(tries > 500)
                    continue;
                var doorPos = Position3D.create(doorSpot,onY,0);
                getBlocks().put(doorPos, new Floor());
                addEntity(new AutomaticDoor(), doorPos);

            }

        }

        if(!doorsPlaced)
        {
            doorsPlaced = true;
            int doorSpot;
            int tries = 0;
            do {
                doorSpot = Random.Default.nextInt(lastFloorStart+1, toArea.getEndX());
                tries++;

            }while(tries<510 && !canBeDoorsX(doorSpot,onY));

            if(tries > 500)
                return;
            var doorPos = Position3D.create(doorSpot,onY,0);
            getBlocks().put(doorPos, new Floor());
            addEntity(new AutomaticDoor(), doorPos);

        }
    }

    //Obtahne zdma vsechny obdelniky co vznikly binari divisionem
    //Je hodne neefektivni, zbytecne vykresluje spoustu veci mockrat.
    private void fillTreeWalls(Tree ofTree)
    {
        if(ofTree == null)
            return;
        wallArea(ofTree.area);
        fillTreeWalls(ofTree.leftChild);
        fillTreeWalls(ofTree.rightChild);
    }

    private void wallArea(Box area)
    {
        for(int x = area.getStartX(); x<area.getEndX(); x++) {
            int y = area.getStartY();
            var block = getBlocks().get(Position3D.create(x,y , 0));
            if(block != null)
                getBlocks().put(Position3D.create(x,y , 0), new Wall());

            y = area.getEndY();
            block = getBlocks().get(Position3D.create(x,y , 0));
            if(block != null)
                getBlocks().put(Position3D.create(x,y , 0), new Wall());

        }
        for(int y = area.getStartY(); y<area.getEndY(); y++)
        {
            int x = area.getStartX();
            var block = getBlocks().get(Position3D.create(x,y , 0));
            if(block != null)
                getBlocks().put(Position3D.create(x,y , 0), new Wall());

            x = area.getEndX();
            block = getBlocks().get(Position3D.create(x,y , 0));
            if(block != null)
                getBlocks().put(Position3D.create(x,y , 0), new Wall());
        }
    }

    //Binary tree splitting
    private Tree splitAreaTree(Box area, int iterations)
    {
        var root = new Tree(area);
        if (iterations == 0)
            return root;
        Box[] split = splitArea(area);
        if(split != null) {
            root.leftChild = splitAreaTree(split[0], iterations - 1);
            root.rightChild = splitAreaTree(split[1], iterations - 1);
            root.splitPoint = split[2].point;
        }

        return root;
    }
    //Vezme obdelnik, a rozdeli ho na dva.
    //Nejdriv si projede vsechny souradnice kde muze delit
    //a koukne jeslti to nekde nevytvori room co je mensi nez min. velikost roomu
    //Kdyz zjisti ze to:
    //  Vytvori aspon jeden novej room
    //  Nevytvori room mensi nez minRoom
    //Tak si ho to prida do listu souradnic pro split. Z nich nahodne jednu vybere.
    private Box[] splitArea(Box area)
    {
        var splitByY = rand.nextBoolean();
        if(splitByY)
            return splitHorizontal(area);
        return splitVertical(area);
    }

    //Split by Y
    private Box[] splitHorizontal(Box area)
    {
        Box top;
        Box bottom;
        Box splitPoint;
        List<Integer> splittableCoords = new ArrayList<Integer>();
        for(int y = area.getStartY(); y<area.getEndY(); y++) {
            if(canBeSplitHorizontal(area,y))
                splittableCoords.add(y);
        }

        if(splittableCoords.size() == 0)
            return null;

        int splitCoord = splittableCoords.get(rand.nextInt(splittableCoords.size()));
        int height = splitCoord - area.getStartY();
        top = new Box(area.getStartX(),area.getStartY(),area.getWidth(),height);
        bottom = new Box(area.getStartX(),area.getStartY()+height,area.getWidth(),area.getHeight() - height);
        splitPoint = new Box(-1,splitCoord,0,0);
        return new Box[] {top,bottom,splitPoint};
    }

    private Box[] splitVertical(Box area)
    {
        Box left;
        Box right;
        Box splitPoint;
        List<Integer> splittableCoords = new ArrayList<Integer>();
        for(int x = area.getStartX(); x<area.getEndX(); x++) {
            if(canBeSplitVertical(area,x))
                splittableCoords.add(x);
        }

        if(splittableCoords.size() == 0)
            return null;

        int splitCoord = splittableCoords.get(rand.nextInt(splittableCoords.size()));
        int width = splitCoord - area.getStartX();
        left = new Box(area.getStartX(),area.getStartY(),width,area.getHeight());
        right = new Box(area.getStartX() + width,area.getStartY(),area.getWidth() - width,area.getHeight());
        splitPoint = new Box(splitCoord,-1,0,0);
        return new Box[] {left,right, splitPoint};
    }

    //Tahle funkce zajistuje prave kontrolu jestli jde danej box splitnout
    //na dany Y souradnici.
    //Projede si celej split, a tam kde deli room koukne, jestli je doleva i doprava
    //vsude dost mista - tj je ten room dost velklej
    private boolean canBeSplitHorizontal(Box area, int splitY)
    {
        double lenToDown = splitY - area.getStartY();
        double lenToUp = area.getEndY() - splitY;
        if(lenToDown < minRoomSize || lenToUp < minRoomSize)
            return false;

        boolean isMeaningful = false;
        for(int x = area.getStartX(); x<area.getEndX(); x++)
        {
            if(isWall(x,splitY))
                continue;
            isMeaningful = true;
            int freeSpaceCount = 0;
            for(int i=1;i<getWidth();i++)
            {
                if(isWall(x,splitY+i) || isWall(x,splitY-i))
                    break;
                freeSpaceCount++;
            }
            if(freeSpaceCount < minRoomSize)
                return false;
        }
        return isMeaningful;
    }

    private boolean canBeSplitVertical(Box area, int splitX)
    {
        double lenToLeft = splitX - area.getStartX();
        double lenToRight = area.getEndX() - splitX;
        if(lenToLeft < minRoomSize || lenToRight < minRoomSize)
            return false;

        boolean isMeaningful = false;
        for(int y = area.getStartY(); y<area.getEndY(); y++)
        {
            if(isWall(splitX,y))
                continue;
            isMeaningful = true;
            int freeSpaceCount = 0;
            for(int i=1;i<getHeight();i++)
            {
                if(isWall(splitX+i,y) || isWall(splitX-i,y))
                    break;
                freeSpaceCount++;
            }
            if(freeSpaceCount < minRoomSize)
                return false;
        }
        return isMeaningful;
    }



}
