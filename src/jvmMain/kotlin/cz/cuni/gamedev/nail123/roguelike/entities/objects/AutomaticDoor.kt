package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom

class AutomaticDoor: GameEntity(GameTiles.CLOSED_DOOR), Interactable {
    val isOpenProperty = createPropertyFrom(false)
    var isOpen by isOpenProperty.asDelegate()

    override val blocksMovement: Boolean
        get() = !isOpen
    override val blocksVision: Boolean
        get() = !isOpen

    init {
        isOpenProperty.onChange { tile = if (isOpen) GameTiles.OPEN_DOOR else GameTiles.CLOSED_DOOR }
    }

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) { open() }
        withEntity<Enemy>(InteractionType.BUMPED) { open() }
        withEntity<Rat>(InteractionType.BUMPED) { open() }
    }

    fun open()
    {
        isOpen = true
        turnsOpen = 0
    }

    fun close()
    {
        isOpen = false;
    }

    var turnsOpen = 0
    val closeAfter = 3
    override fun update() {
        super.update()
        var isOcupied = area[position]?.entities?.any{it !is AutomaticDoor}
        if(isOcupied != null && isOcupied) {
            open()
            return
        }
        turnsOpen++;
        if(turnsOpen > closeAfter)
            close()
    }
}