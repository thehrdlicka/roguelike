package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Cheese(var hpHeal: Int): Item(GameTiles.CHEESE) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        return if (area.player.hitpoints == area.player.maxHitpoints) {
            Inventory.EquipResult(false, "Already at full health")
        } else Inventory.EquipResult.Success
    }



    override fun onEquip(character: HasInventory) {
        area.player.heal(hpHeal)
        logMessage("Ate cheese and gained $hpHeal HP")
        character.removeItem(this)
    }

    override fun onUnequip(character: HasInventory) {
        return
    }




    override fun toString(): String {
        return "Cheese"
    }
}