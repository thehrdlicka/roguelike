package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Lighter(var usesLeft: Int): Item(GameTiles.LIGHTER) {
    override fun isEquipable(character: HasInventory): Inventory.EquipResult {
        return if (character.inventory.equipped.filterIsInstance<Lighter>().isNotEmpty()) {
            Inventory.EquipResult(false, "Cannot equip two lighters")
        } else Inventory.EquipResult.Success
    }



    override fun onEquip(character: HasInventory) {
        logMessage("Equipped Lighter. Press 'F' to light a fire.")
    }

    override fun onUnequip(character: HasInventory) {
        return
    }

    fun CanUse() : Boolean
    {
        if(usesLeft <= 0)
            return false

        return true
    }

    fun UsesLeft() : Int{ return usesLeft}

    fun TryUse() : Boolean
    {
        if(usesLeft <= 0)
            return false
        usesLeft-= 1
        return true
    }


    override fun toString(): String {
        return "Lighter|$usesLeft uses"
    }
}