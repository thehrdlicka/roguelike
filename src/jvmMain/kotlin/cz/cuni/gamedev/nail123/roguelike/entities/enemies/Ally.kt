package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.MovingEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.*
import cz.cuni.gamedev.nail123.roguelike.entities.items.Lighter
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Airlock
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent
import cz.cuni.gamedev.nail123.roguelike.mechanics.Combat
import org.hexworks.zircon.api.data.Tile
import kotlin.random.Random

abstract class Ally(tile: Tile): MovingEntity(tile), HasCombatStats, Interactable, Interacting {
    override val blocksMovement = true

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Enemy>(InteractionType.BUMPED) { player -> Combat.attack(player, this@Ally) }
        withEntity<Player>(InteractionType.BUMPED) { player ->
            var myPos = position
            moveTo(player.position)
            player.moveTo(myPos)}
    }

    override fun interactWith(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Enemy>(InteractionType.BUMPED) { player -> Combat.attack(this@Ally, player) }
    }

    override fun update() {
        if(dead)
            return

        super.update()
        hasSuffocatedThisTurn = false
    }

    var hasSuffocatedThisTurn = false

    open fun Suffocate(fromAirlock : Airlock, damage : Int)
    {
        if(hasSuffocatedThisTurn)
            return

        hasSuffocatedThisTurn = true
        takeDamage(damage)
        LoggedEvent(this, "$this is sufocating for $damage damage.").emit()

    }

    override fun die() {
        Destroy()

    }
}