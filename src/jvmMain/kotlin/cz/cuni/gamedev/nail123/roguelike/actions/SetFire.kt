package cz.cuni.gamedev.nail123.roguelike.actions

import cz.cuni.gamedev.nail123.roguelike.entities.items.Item
import cz.cuni.gamedev.nail123.roguelike.entities.items.Lighter
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Fire
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.world.Area

class SetFire: GameAction() {
    override fun tryPerform(area: Area): Boolean {
        val playerPos = area.player.position
        val lighter : Lighter? = area.player.inventory.equipped.filterIsInstance<Lighter>().firstOrNull()
        if(lighter == null) {
            logMessage("Cannot light a fire. I don't have a lighter equipped.")
            return false
        } else if(!lighter.CanUse())
        {
            logMessage("Cannot light a fire. My lighter is empty.")
            return false
        }

        val postToLight = Pathfinding.eightDirectional(playerPos).firstOrNull{val result = area[it]?.blocksMovement
        result != null && !result}
        if(postToLight != null) {
            area.addEntity(Fire(), postToLight)
            logMessage("Lighting a fire. ${lighter.UsesLeft()} matches left.")
            lighter.TryUse()
            return true
        }

        logMessage("There is no space around me to light a fire.")
        return false
    }
}