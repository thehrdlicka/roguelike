package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding.defaultBlocking
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding.eightDirectional
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding.floodFill
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding.floorOnly
import cz.cuni.gamedev.nail123.roguelike.mechanics.goSmartlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom
import kotlin.random.Random

class Airlock: GameEntity(GameTiles.CLOSED_DOOR), Interactable {
    val isOpenProperty = createPropertyFrom(false)
    var isOpen by isOpenProperty.asDelegate()

    override val blocksMovement = true
    override val blocksVision = true

    val damagePerTurn = 7
    var maxSuckTime = -1

    init {
        isOpenProperty.onChange { tile = if (isOpen) GameTiles.OPEN_DOOR else GameTiles.CLOSED_DOOR }

    }

    override fun acceptInteractFrom(other: GameEntity, type: InteractionType) = interactionContext(other, type) {
        withEntity<Player>(InteractionType.BUMPED) { toggle() }
        withEntity<Enemy>(InteractionType.BUMPED) { if(other is Enemy) {
            suckOut(other)
        } }
    }

    fun toggle()
    {
        isOpen = !isOpen
        turnsOpen = 0
    }

    fun isSucking() : Boolean
    {
        return isOpen && turnsOpen < maxSuckTime
    }

    fun suckOut(other : Enemy)
    {
        if(!isOpen)
            return
        if(!isSucking() || Random.nextFloat() < 0.1) {
            LoggedEvent(this, "$other has barely managed to close the airlock!").emit()
            toggle()
            return
        }
        other.takeDamage(99999)

        LoggedEvent(this, "$other has been sucked out of the airlock!").emit()
    }
    var turnsOpen = 0

    override fun update() {
        super.update()
        if(maxSuckTime == -1)
        {
            val floodFill = floodFill(
                    position,
                    area,
                    eightDirectional,
                    defaultBlocking
            )
            maxSuckTime = floodFill.values.last()
        }
        if(isOpen)
        {
            turnsOpen += 1
            val floodFill = floodFill(
                    position,
                    area,
                    eightDirectional,
                    floorOnly
            )

            floodFill.forEach {
                    area.fetchBlockAt(it.key).get().entities.toTypedArray().forEach { ent ->
                        if(ent is Enemy) {
                            ent.Suffocate(this, damagePerTurn)
                            if(turnsOpen < maxSuckTime)
                                ent.goSmartlyTowards(position)
                        }
                        if(ent is Fire)
                            ent.RemoveAir()
                        }
                    if(it.key.equals(area.player.position))
                        area.player.Sufocate(this)
            }
            }
        }
    }
