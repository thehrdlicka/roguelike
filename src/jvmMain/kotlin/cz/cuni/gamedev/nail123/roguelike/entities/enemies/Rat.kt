package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Airlock
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Fire
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.Vision
import cz.cuni.gamedev.nail123.roguelike.mechanics.goBlindlyTowards
import cz.cuni.gamedev.nail123.roguelike.mechanics.goSmartlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Position
import org.hexworks.zircon.api.data.Position3D
import kotlin.random.Random

class Rat: Ally(GameTiles.RAT), HasSmell, HasVision {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 10
    override val visionRadius = 10

    override val maxHitpoints = 10
    override var hitpoints = 6
    override var attack = 3
    override var defense = 3

    val wanderTimeMin = 3
    val wanderTimeMax = 10


    var theWanderTarget: Position3D = Position3D.defaultPosition()
    var theWanderOffset:Position3D = Position3D.defaultPosition()
    var frameCount = 0

    override fun update() {
        super.update()
        frameCount+=1

        var nearEnemies = Pathfinding.eightDirectional(position).filter { area[it] != null && area[it]!!.entities.any{ent -> ent is Enemy} }

        if(nearEnemies.size > 0)
        {
            goBlindlyTowards(nearEnemies[Random.nextInt(nearEnemies.size)])
            return
        }

        val canSee = Vision.getVisiblePositionsFrom(area,position,visionRadius)

        //Attack enemies we see
        val seeEnemies = canSee.filter{ pos ->
            val result = area.get(pos)?.entities?.any{ent -> ent is Enemy }
            result != null && result}
        val seeEnemyPos = seeEnemies.minBy { it -> Pathfinding.chebyshev(position,it) }
        if(seeEnemyPos != null)
        {
            goSmartlyTowards(seeEnemyPos)
            return
        }

        wander()
    }

    override fun die() {
        super.die()
    }

    var wanderChangeFrame = 0

    fun wander()
    {
        if(frameCount >= wanderChangeFrame)
        {
            wanderChangeFrame =  frameCount + Random.nextInt(wanderTimeMin,wanderTimeMax)
            theWanderOffset = getWanderOffset()
        }
        theWanderTarget = getWanderTarget()

        goSmartlyTowards(theWanderTarget!!)
    }

    fun getWanderOffset() : Position3D
    {
        var positions = Vision.getVisiblePositionsFrom(area,area.player.position,5).filter{!(area[it]!!.blocksMovement)};
        return area.player.position - positions[Random.nextInt(positions.size)]
    }

    fun getWanderTarget() : Position3D
    {
        var tarPos = area.player.position + theWanderOffset
        var count = 0
        while(count < 50 && (area[tarPos] == null || area[tarPos]!!.blocksMovement || area[tarPos]!!.entities.any{ent -> ent is Fire}))
            {
                count+=1
                theWanderOffset = getWanderOffset()
                tarPos = area.player.position + theWanderOffset
            }
        if(count >= 50)
            tarPos = area.player.position
        return tarPos
    }

}