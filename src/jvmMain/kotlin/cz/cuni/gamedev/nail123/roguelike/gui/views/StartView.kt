package cz.cuni.gamedev.nail123.roguelike.gui.views

import org.hexworks.zircon.api.ColorThemes
import org.hexworks.zircon.api.ComponentDecorations
import org.hexworks.zircon.api.Components
import org.hexworks.zircon.api.component.ComponentAlignment
import org.hexworks.zircon.api.graphics.BoxType
import org.hexworks.zircon.api.grid.TileGrid
import org.hexworks.zircon.api.uievent.ComponentEventType
import org.hexworks.zircon.api.uievent.UIEventResponse
import org.hexworks.zircon.api.view.base.BaseView

class StartView(val tileGrid: TileGrid): BaseView(tileGrid, ColorThemes.arc()) {
    override fun onDock() {
        val msg = "In the grim darkness of the distant future"
        val msg2 = "a group of Rattasites has found"
        val msg3 = "a pack of matches..."
        val header = Components.textBox(msg.length)
                .addHeader(msg)
                .addNewLine()
                .withAlignmentWithin(screen, ComponentAlignment.CENTER)
                .build()
        val header2 = Components.textBox(msg.length)
                .addHeader(msg2)
                .addNewLine()
                .withAlignmentAround(header, ComponentAlignment.BOTTOM_CENTER)
                .build()

        val header3 = Components.textBox(msg.length)
                .addHeader(msg3)
                .addNewLine()
                .withAlignmentAround(header2, ComponentAlignment.BOTTOM_CENTER)
                .build()

        val startButton = Components.button()
                // we align the button to the bottom center of our header
                .withAlignmentAround(header3, ComponentAlignment.BOTTOM_CENTER)
                .withText("Start!") // its text is "Start!"
                .withDecorations(
                        ComponentDecorations.box(BoxType.SINGLE),
                        ComponentDecorations.shadow()
                )
                .build()

        startButton.handleComponentEvents(ComponentEventType.ACTIVATED) {
            // Goto play_view
            replaceWith(PlayView(tileGrid))
            UIEventResponse.processed()
        }

        screen.addComponent(header)
        screen.addComponent(header2)
        screen.addComponent(header3)
        screen.addComponent(startButton)
    }
}