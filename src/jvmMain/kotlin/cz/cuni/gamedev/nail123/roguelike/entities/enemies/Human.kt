package cz.cuni.gamedev.nail123.roguelike.entities.enemies

import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasSmell
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision
import cz.cuni.gamedev.nail123.roguelike.entities.items.Lighter
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Airlock
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Fire
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding.eightDirectional
import cz.cuni.gamedev.nail123.roguelike.mechanics.Vision
import cz.cuni.gamedev.nail123.roguelike.mechanics.goSmartlyTowards
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.zircon.api.data.Position
import org.hexworks.zircon.api.data.Position3D
import kotlin.random.Random

class Human: Enemy(GameTiles.ENEMY), HasSmell, HasVision {
    override val blocksMovement = true
    override val blocksVision = false
    override val smellingRadius = 15
    override val visionRadius = 15

    override val maxHitpoints = 100
    override var hitpoints = 100
    override var attack = 5
    override var defense = 0

    val extinguishPerTurn = 1
    val wanderPauseMin = 3
    val wanderPauseMax = 10

    var lastPlayerPos: Position3D? = null
    var smelledFire : Position3D? = null
    var closeAirlock: Airlock? = null
    var theWanderTarget: Position3D? = null
    var frameCount = 0
    var lastSeenPlayer = 0

    override fun update() {
        frameCount+=1
        if(dead)
            return

        super.update()
        val canSee = Vision.getVisiblePositionsFrom(area,position,visionRadius)

        //Update player position, so we can try finding him if we have nothing more important to do.
        val seePlayerPos = canSee.contains(area.player.position)
        if(seePlayerPos) {
            lastPlayerPos = area.player.position
            lastSeenPlayer = frameCount
        } else if(lastPlayerPos != null && frameCount - lastSeenPlayer < 5)
        {
            lastPlayerPos = area.player.position
        }

        if(lastPlayerPos != null && !seePlayerPos && Pathfinding.chebyshev(position, lastPlayerPos!!) <= 1)
            lastPlayerPos = null



        //If there is a player nearby, hit him.
        if (Pathfinding.chebyshev(position, area.player.position) <= 1) {
            goSmartlyTowards(area.player.position)
            return
        }

        if(closeAirlock != null && !closeAirlock!!.isOpen)
            closeAirlock = null

        //If we are being sucked by airlock, dont do anything.
        if(closeAirlock != null && closeAirlock!!.isSucking())
            return

        //If there is any fire nearby, extinguish it. Also check for rats.
        var firesNearby : MutableList<Fire> = mutableListOf()
        var ratsNearby : MutableList<Rat> = mutableListOf()
        eightDirectional(position).forEach{
            area.blocks[it]?.entities?.forEach{ent ->
                if(ent is Fire)
                    firesNearby.add(ent)
            if(ent is Rat)
            ratsNearby.add(ent)}
        }


            firesNearby.take(extinguishPerTurn).forEach { ent -> ent.Extinguish()}

        if(firesNearby.count() >= 6) {
            LoggedEvent(this, "$this is burning for ${firesNearby[0].fireDamage} damage.").emit()
            takeDamage(firesNearby[0].fireDamage)
        }

        if(firesNearby.count() > 0)
            return


        //If we see fire, extinguish it!
        val seeFires = canSee.filter{ pos ->
            val result = area.get(pos)?.entities?.any{ent -> ent is Fire}
        result != null && result}
        val seeFirePos = seeFires.minBy { it -> Pathfinding.chebyshev(position,it) }
        if(seeFirePos != null)
        {
            smelledFire = seeFirePos
            if(Pathfinding.chebyshev(position,seeFirePos) > 1)
                goSmartlyTowards(seeFirePos)
            return
        }

        //If we see open airlock, go close it.
        val seeAirlock = canSee.firstOrNull{ pos ->
            val result = area.get(pos)?.entities?.any{ent -> ent is Airlock && ent.isOpen }
            result != null && result}
        if(seeAirlock != null)
        {
            goSmartlyTowards(seeAirlock)
            return
        }

        if (closeAirlock != null)
        {
                goSmartlyTowards(closeAirlock!!.position)
                return

        }

        //If there is a rat nearby, kill it.
        var nearRat = ratsNearby.firstOrNull()
        if(nearRat != null)
        {
            goSmartlyTowards(nearRat.position)
            return
        }

        //If we can see player, go kill it.
        if(seePlayerPos)
        {
            goSmartlyTowards(area.player.position)
            return
        }

        //If we can smell fire, go towards it.
        val smellFire = Pathfinding.floodFillFindFire(position, area,eightDirectional, {_ : GameBlock -> false}, smellingRadius )
        if(smellFire != null) {
            smelledFire = smellFire.position
            goSmartlyTowards(smellFire.position)
            return
        } else if(smelledFire != null)
        {
            if(!canSee.contains(smelledFire!!)) {
                goSmartlyTowards(smelledFire!!)
                return
            } else
                smelledFire = null
        }

        //Look at last known player location to check if
        //If we can find player.
        if(lastPlayerPos != null)
        {
            goSmartlyTowards(area.player.position)
            return
        }

        //Otherwise just wander randomly
        wander()

    }


    var wanderStartFrame = 0
    var lastWanderFrame = 0

    fun wander()
    {
        if(theWanderTarget == null)
            theWanderTarget = getWanderTarget()

        if(Pathfinding.chebyshev(position, theWanderTarget!!) < 3) {
            theWanderTarget = getWanderTarget()
            wanderStartFrame =  frameCount+Random.nextInt(wanderPauseMin,wanderPauseMax)
        }

        if(frameCount - lastWanderFrame > 5)
        {
            wanderStartFrame =  frameCount+Random.nextInt(wanderPauseMin,wanderPauseMax)
        }

        if(frameCount >= wanderStartFrame)
            goSmartlyTowards(theWanderTarget!!)


        lastWanderFrame=frameCount
    }

    fun getWanderTarget() : Position3D
    {
        var randomPos:Position3D
        do {
            var randomRoom = area.rooms[Random.nextInt(area.rooms.size)]
            var x = Random.nextInt(randomRoom.startX+1,randomRoom.endX-1)
            var y = Random.nextInt(randomRoom.startY+1,randomRoom.endY-1)
            randomPos = Position3D.create(x,y,0)
        }while(area[randomPos] == null || !area[randomPos]!!.blocksMovement);

        return randomPos
    }

    override fun Suffocate(fromAirlock : Airlock, damage : Int)
    {
        super.Suffocate(fromAirlock, damage)
        if(hasSuffocatedThisTurn)
            return

        closeAirlock = fromAirlock

    }

    override fun die() {
        super.die()


        var lighterCount = Random.nextInt(2,4)
        if(Random.nextFloat() < 0.4)
            this.block.entities.add(Lighter(lighterCount))

        if(Random.nextFloat() < 0.7) {
            area.addEntity(Rat(), position)
            LoggedEvent(this, "Another Rattasite has burst out of the corpse").emit()
        }
    }
}