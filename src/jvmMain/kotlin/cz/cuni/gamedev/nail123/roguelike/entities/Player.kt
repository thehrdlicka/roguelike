package cz.cuni.gamedev.nail123.roguelike.entities

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item
import cz.cuni.gamedev.nail123.roguelike.entities.items.Lighter
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Airlock
import cz.cuni.gamedev.nail123.roguelike.events.GameOver
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent
import cz.cuni.gamedev.nail123.roguelike.events.logMessage
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Player: MovingEntity(GameTiles.RATKING), HasVision, HasCombatStats, HasInventory {
    override val visionRadius = 9
    override val blocksMovement = true
    override val blocksVision = false

    override var maxHitpoints = 30
    override var hitpoints = 30
    override var attack = 2
    override var defense = 1

    var sufocateDamage = 1
    override val inventory = Inventory(this)



    override fun die() {
        Destroy()
        this.logMessage("You have died!")
        GameOver(this).emit()
    }

    override fun update() {
        super.update()
        hasSuffocatedThisTurn = false
    }

    var hasSuffocatedThisTurn = false

    fun Sufocate(airlock: Airlock) {
        if(hasSuffocatedThisTurn)
            return
        hasSuffocatedThisTurn = true
        takeDamage(sufocateDamage)
        LoggedEvent(this, "You are sufocating for $sufocateDamage damage!").emit()
    }
}