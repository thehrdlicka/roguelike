package cz.cuni.gamedev.nail123.roguelike.entities.objects

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor
import cz.cuni.gamedev.nail123.roguelike.entities.GameEntity
import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Interactable
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.InteractionType
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.interactionContext
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent
import cz.cuni.gamedev.nail123.roguelike.mechanics.Pathfinding
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles
import org.hexworks.cobalt.databinding.api.extension.createPropertyFrom
import kotlin.random.Random

class Fire: GameEntity(GameTiles.FIRE) {


    override val blocksMovement=  false
    override val blocksVision = false
    val timeToSpread = 10
    val fireDamage = 5;
    val noAirDecay = 5

    var lifeTime = 0
    var currentSpread = 0
    override fun update() {
        super.update()
        area.blocks[position]?.entities?.filterIsInstance<HasCombatStats>()?.forEach{
            it.takeDamage(fireDamage)
            LoggedEvent(this, "$it is burning for $fireDamage damage.").emit()
        }

        lifeTime += 1
        currentSpread += 1
        if(currentSpread < timeToSpread)
            return

        SpreadFire()
        currentSpread = 0
    }

    fun SpreadFire()
    {
        val emptyDirs = Pathfinding.eightDirectional(position).filter {
            area.blocks.get(it) != null && area.blocks[it] is Floor && !(area.blocks[it]!!.entities.any { ent : GameEntity  -> ent is Fire || ent is Enemy || (ent is AutomaticDoor && ent.blocksMovement) })
        }
        if(emptyDirs.count() > 0)
            area.addEntity(Fire(),emptyDirs[Random.nextInt(emptyDirs.count())])
    }

    fun RemoveAir()
    {
        lifeTime -= noAirDecay
        currentSpread = 0
        if(lifeTime <= 0)
            Extinguish()
    }

    fun Extinguish() {
        (this as GameEntity?)?.area?.removeEntity(this)
    }


}